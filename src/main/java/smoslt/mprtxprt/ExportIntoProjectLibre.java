/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.mprtxprt module, one of many modules that belongs to smoslt

 smoslt.mprtxprt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.mprtxprt is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.mprtxprt in the file smoslt.mprtxprt/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.mprtxprt;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.btrg.uti.DateUtils_;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projity.field.FieldParseException;
import com.projity.grouping.core.Node;
import com.projity.grouping.core.NodeFactory;
import com.projity.grouping.core.model.DefaultNodeModel;
import com.projity.pm.assignment.Assignment;
import com.projity.pm.resource.Resource;
import com.projity.pm.resource.ResourceImpl;
import com.projity.pm.resource.ResourcePool;
import com.projity.pm.resource.ResourcePoolFactory;
import com.projity.pm.scheduling.ConstraintType;
import com.projity.pm.task.NormalTask;
import com.projity.pm.task.Project;
import com.projity.pm.task.Task;
import com.projity.undo.DataFactoryUndoController;
import com.projity.util.Environment;

public class ExportIntoProjectLibre {
	private smoslt.domain.Schedule schedule;
	private double units = 1D;

	private ExportIntoProjectLibre() {

	}

	public ExportIntoProjectLibre(smoslt.domain.Schedule schedule) {
		this.schedule = schedule;
		printStartDays(schedule);
	}

	private void printStartDays(smoslt.domain.Schedule schedule) {
		System.out.print("EXPORTING ");
		for (smoslt.domain.Task task : schedule.getTaskList()) {
			System.out.print(" " + task.getStartDay());
		}
		System.out.println();
	}

	private Project project;
	private com.projectlibre.pm.tasks.Project plProject = new com.projectlibre.pm.tasks.Project();
	private File dir = new File("generated");
	private long now = new Date().getTime();
	ResourcePool resourcePool;
	protected Map<com.projity.pm.resource.Resource, Node> openprojResourceNodeMap = new HashMap<com.projity.pm.resource.Resource, Node>();

	public Project getProject() {
		return project;
	}

	public void go() {
		dir.mkdir();
		Environment.setImporting(true);
		Environment.setStandAlone(true);
		DataFactoryUndoController undoController = new DataFactoryUndoController();
		resourcePool = ResourcePoolFactory.getInstance().createResourcePool("",
				undoController);
		resourcePool.setLocal(true);
		project = Project.createProject(resourcePool, undoController);
		((DefaultNodeModel) project.getTaskOutline()).setDataFactory(project);
		project.initializeProject();
		setProjectProfile();
		project.setName("myProject");
		addResources();
		addTasks();
		// addAssignments(); called when adding tasks
		Environment.setImporting(false);
		/* START trying things to clear start error dialog */
		// project.setAllDirty();
		project.setForward(false);
		// project.setTemporaryLocal(true);
		// project.resetRoles(true);
		// project.recalculate();
		/* END trying things to clear start error dialog */
		// System.out.println("SCHEDULING METHOD "+
		// project.getSchedulingMethod());
		// project.setBoundsAfterReadProject();
	}

	// private void addAssignments() {
	// // for (Task task : plProject.getTasks()) {
	// // NormalTask openprojTask = state.getOpenprojTask(task);
	// // for (com.projectlibre.pm.tasks.Assignment assignment : task
	// // .getAssignments()) {
	// // Assignment openprojAssignment = converter.to(assignment, state);
	// // openprojTask.addAssignment(openprojAssignment);
	// // }
	// // }
	// }

	void setProjectProfile() {
		try {
			project.setNotes(new ObjectMapper().writeValueAsString(schedule
					.getProjectProfile()));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		project.setNotes("sOMEthing\n" + project.getNotes() + "\n"
				+ schedule.getClass() + "\n" + schedule.getProjectProfile());
	}

	private void addResources() {
		resourcePool.setLocal(true);
		resourcePool.setMaster(false);
		resourcePool.updateOutlineTypes();
		for (smoslt.domain.Resource _resource : schedule.getResources()) {
			addResource(_resource.getName(), _resource.getGroup(),
					_resource.getId());
		}
		// addResource("Jimmy Johnson", "Tango", 0L);
		// addResource("Harry Belusky", "Tango", 1L);
		// addResource("Bull Wonkers", "Tango", 2L);
	}

	private void addResource(String name, String group, long id) {
		ResourceImpl openprojResource;
		openprojResource = resourcePool.newResourceInstance();
		openprojResource.setName(name);
		openprojResource.setGroup(group);
		openprojResource.setId(id);
		openprojResource.setWorkCalendar(project.getBaseCalendar());
		openprojResource.setEffortDriven(true);
		openprojResource.setLocal(true);
		Node openprojResourceNode = NodeFactory.getInstance().createNode(
				openprojResource);
		resourcePool.addToDefaultOutline(null, openprojResourceNode);
		openprojResourceNodeMap.put(openprojResource, openprojResourceNode);
	}

	public void export(File file) {
		// File file = new File(dir, UUID.randomUUID().toString().substring(0,
		// 8) + ".pod");
		FileImportExport fileImportExport = new FileImportExport();
		fileImportExport.exportProject(file.getAbsolutePath(), project);
	}

	public void addTasks() {
		for (smoslt.domain.Task _task : schedule.getTaskList()) {
			Task thisTask = project.newNormalTaskInstance(true);
			thisTask.setId(_task.getId());
			/*
			 * bunch of hairy code to make a list that looks like this
			 * "3,6,19,4"
			 */
			if (null != _task.getPredecessors()) {
				StringBuffer sb = new StringBuffer();
				for (smoslt.domain.Task _predecessor : _task.getPredecessors()) {
					if (_predecessor.getId() != 0) {
						sb.append(_predecessor.getId() + ";");
					}
				}
				String predecessors = sb.toString().trim();
				if (predecessors.endsWith(";")) {
					predecessors = predecessors.substring(0,
							predecessors.length() - 1);
				}
				try {
					thisTask.setPredecessors(predecessors);
				} catch (FieldParseException e) {
					// throw new RuntimeException(e);
					System.err.println("ERROR ON " + thisTask.getName() + "\n"
							+ e.getMessage());
				}
			}
			// System.out.println("SHOULD BE SEEING START DAY OF "+
			// _task.getStartDay() + " FOR "+ _task.getName());
			addTask(_task.getName(), _task.getStartDay(),
					_task.getDurationHours(), thisTask, null);
			assignResources(thisTask);
		}
		// Task thisTask = project.newNormalTaskInstance(true);
		// thisTask.setId(1L);
		// Task thatTask = project.newNormalTaskInstance(true);
		// thatTask.setId(2L);
		// try {
		// thatTask.setPredecessors("" + 1L);
		// } catch (FieldParseException e) {
		// throw new RuntimeException(e);
		// }
		// Node newTaskNode = addTask("Start out the project", 5, thisTask,
		// null);
		// Node newTaskNode2 = addTask("Then, there is this", 5, thatTask,
		// null);
	}

	public Node addTask(String name, int startDay, int hours, Task task,
			Node parentTask) {
		// System.out.println(" START DAY " + startDay);
		Date tomorrow = DateUtils_.followingDay(new Date(), 1);
		long startDayL = tomorrow.getTime() + (startDay * 28800000L);
		task.setName(name);
		task.setDuration(3600000L * hours);
		((NormalTask) task).setEstimated(true);
		task.setOwningProject(project);
		task.setProjectId(project.getUniqueId());
		((NormalTask) task).setStart(startDayL);
		task.setLevelingDelay(Math.abs((startDay * 28800000L)));
		try {
			task.setConstraintType(ConstraintType.SNET);
		} catch (FieldParseException e) {
			e.printStackTrace();
		}
		Node openprojTaskNode = NodeFactory.getInstance().createNode(task);
		project.addToDefaultOutline(parentTask, openprojTaskNode);
		assignResources(task);
		project.add(task);
		return openprojTaskNode;
	}

	private void assignResources(Task task) {
		for (Resource resource : getResources(task)) {
			assignResource(task, resource);
		}
		task.setScheduleConstraint(ConstraintType.SNET, now);
	}

	private void assignResource(Task task, Resource resource) {
		Assignment assignment = Assignment.getInstance(task, resource, units,
				0L);
		NormalTask myTask = (NormalTask) task;
		myTask.addAssignment(assignment);
	}

	private List<Resource> getResources(Task task) {
		List<Resource> myResources = new ArrayList<Resource>();
		for (long taskId : schedule.getAssignments().keySet()) {
			if (taskId == task.getId()) {
				for (Long resourceId : schedule.getAssignments().get(taskId)) {
					List<Resource> resources = project.getResourcePool()
							.getResourceList();
					for (Resource resource : resources) {
						long resource_Id = resource.getId();
						if (resource_Id == resourceId) {
							myResources.add(resource);
						}
					}
				}
			}
		}
		return myResources;
	}

}
