/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.mprtxprt module, one of many modules that belongs to smoslt

 smoslt.mprtxprt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.mprtxprt is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.mprtxprt in the file smoslt.mprtxprt/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.mprtxprt;



import com.projity.exchange.LocalFileImporter;
import com.projity.pm.task.Project;
import com.projity.pm.task.ProjectFactory;


public class FileImportExport {

	public Project importFile(String filePath) {
		LocalFileImporter fileImporter = new LocalFileImporter();
		fileImporter
				.setFileName(filePath);
		try {
			fileImporter.importFile();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		Project project = fileImporter.getProject();
		return project;
	}
	
	public void exportProject(String filePath, Project project){
		LocalFileImporter fileImporter = new LocalFileImporter();
		fileImporter = new LocalFileImporter();
		fileImporter
				.setFileName(filePath);
		fileImporter.setProjectFactory(ProjectFactory.getInstance());
		fileImporter.setProject(project);
		try {
			fileImporter.exportFile();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
