/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.mprtxprt module, one of many modules that belongs to smoslt

 smoslt.mprtxprt is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.mprtxprt is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.mprtxprt in the file smoslt.mprtxprt/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.mprtxprt;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import smoslt.domain.Task;

import com.projity.association.Association;
import com.projity.pm.resource.Resource;
import com.projity.pm.scheduling.SchedulingType;
import com.projity.pm.task.NormalTask;
import com.projity.pm.task.Project;

public class ImportFromProjectLibre implements smoslt.domain.ScheduleBuild {
	/*
	 * The work of this class is to convert com.projity.pm.Resource into
	 * smoslt.domain.Resource, and also com.projity.pm.Task into
	 * smoslt.domain.Task. Same class name, different packages!!! This makes for
	 * a very confusing codebase in a few places. If you prepare yourself in
	 * advance, it can be slightly less confusing.
	 */
	private Project project;
	private Map<Long, smoslt.domain.Task> tasks;
	private Set<smoslt.domain.Resource> resources = new HashSet<smoslt.domain.Resource>();

	public ImportFromProjectLibre(File file) {
		Project project = new FileImportExport().importFile(file
				.getAbsolutePath());
		this.project = project;
		addResources();
		/*
		 * This loops through the tasks twice, first to aggregate all the tasks
		 * themselves and stuff them in the tasks map. Only when you have done
		 * this can you then use this same map of tasks to select the
		 * predecessors and add each predecessor back to it's respective task
		 */
		addTasks();
		/*
		 * Now we have all the tasks, everything is done except adding the
		 * predecessors. Now we do that.
		 */
		addPredecessors();
	}

	private void addPredecessors() {
		for (Object task : project.getTasks().toArray()) {
			NormalTask aTask = (NormalTask) task;
			// System.out.println(""+aTask.get);
			// System.out.println("TASK " + aTask.getName() + " ID "
			// + aTask.getId() + " HAS PREDECESSORS OF "
			// + aTask.getPredecessors());
			smoslt.domain.Task myTask = tasks.get(aTask.getId());
			String predecessors = aTask.getPredecessors();
			StringTokenizer stk = new StringTokenizer(predecessors, ";");
			List<smoslt.domain.Task> predecessorTasks = new ArrayList<smoslt.domain.Task>();
			if (predecessors != null && predecessors.length() > 1) {
				while (stk.hasMoreTokens()) {
					String id = stk.nextToken();
					long taskId = Long.valueOf(id);
					smoslt.domain.Task myPredecessor = tasks.get(taskId);
					myTask.addPredecessor(myPredecessor);
				}
			}
		}
	}

	private void addTasks() {
		tasks = new HashMap<Long, smoslt.domain.Task>();
		for (Object task : project.getTasks().toArray()) {
			String resourceRequirement = "";
			NormalTask aTask = (NormalTask) task;
			aTask.setSchedulingType(SchedulingType.FIXED_WORK);
			aTask.setEffortDriven(true);
			// System.out.println("TASK NAME: " + aTask.getName() + ", TASK:"
			// + aTask.getActualDuration() + ", TO: "
			// + aTask.getEffectiveWorkCalendar().getName());
			// System.out.println("TASK NOTES: " + aTask.getNotes());
			// Assignment myassignment = Assignment.getInstance(aTask, resource,
			// aTask.getUnits(), 0);
			// System.out.println("ASSIGNED TO " + myassignment.get);
			List<Association> associations = aTask.getAssignments();
			for (Association association : associations) {
				// System.out.println(association.getLeft().getClass().getName()
				// + " " + association.getRight().getClass().getName());
				if (association.getRight() instanceof com.projity.pm.resource.ResourceImpl) {
					Resource myResource = (Resource) association.getRight();
					// System.out.println("NAME " + myResource.getName()
					// + " GROUP " + myResource.getGroup());
					if (!"Unassigned".equals(myResource.getName())) {
						/*
						 * PLEASE NOTE THAT THIS IMPORT does not actually create
						 * assignments, that must be done by a follow-up program
						 * such as Stacker. Instead, what it does is write
						 * resourceRequirement string, which should look
						 * something like '1 DevImpl'. This string is then mined
						 * converted into assignments later AFTER import
						 */
						resourceRequirement = resourceRequirement
								+ myResource.getName() + ",";
					} else {
						String taskNamePrefix = extractTaskNamePrefix(aTask
								.getName());
						resourceRequirement = addDefaultResources(
								resourceRequirement, taskNamePrefix);
						// System.out.println("\tRESOURCE REQUIREMENT "
						// + resourceRequirement);
					}
				}
			}
			if (resourceRequirement.endsWith(",")) {
				resourceRequirement = resourceRequirement.substring(0,
						resourceRequirement.length() - 1);
			}
			if (aTask.getDuration() >= 28800000) {
				smoslt.domain.Task myTask = new smoslt.domain.Task(
						(int) aTask.getDuration() / 3600000,
						(int) aTask.getStart() / 28800000, aTask.getName(),
						resourceRequirement);
				tasks.put(aTask.getId(), myTask);
				// System.out.println("\tRESOURCE REQUIREMENT "
				// + resourceRequirement);
				// } else {
				// System.err.println("WEIRD TASK WITH DURATION OF "
				// + aTask.getDuration() + " " + aTask.getName());
			}
		}
	}

	private String extractTaskNamePrefix(String name) {
		if (name.contains(":")) {
			return name.substring(0, name.indexOf(":"));
		}
		return null;
	}

	private String addDefaultResources(String resourceRequirement,
			String taskNamePrefix) {
		if (isMissingResourceSpecifications(resourceRequirement)) {
			return assignDefaultSpecifications(taskNamePrefix);
		} else {
			return resourceRequirement;
		}
	}

	private boolean isMissingResourceSpecifications(String resourceRequirement) {
		if (null == resourceRequirement) {
			return true;
		} else if (resourceRequirement.trim().length() > 3) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * returns first instance of a resource that has a group name which the
	 * taskNamePrefix starts with
	 */
	private String assignDefaultSpecifications(String taskNamePrefix) {
		String resourceRequirement = "";
		if (taskNamePrefix != null && taskNamePrefix.trim().length() > 2) {
			for (smoslt.domain.Resource thisResource : resources) {
				if (!thisResource.getName().contains(",")
						&& taskNamePrefix.toLowerCase().startsWith(
								thisResource.getGroup().trim().toLowerCase())) {
					resourceRequirement = thisResource.getName();
					break;
				}
			}
		}
		return resourceRequirement;
	}

	private void addResources() {
		Iterable<Resource> resources = project.getResourcePool()
				.getResourceList();
		if (null == resources || !resources.iterator().hasNext()) {
			throw new RuntimeException("no resources");
		}
		for (Resource resource : resources) {
			// System.out.println("RESOURCE NAME: " + resource.getName()
			// + ", GROUP:" + resource.getGroup() + " "
			// + resource.getAssignments().size());
			this.resources.add(new smoslt.domain.Resource(resource.getName()
					.trim(), resource.getGroup().trim()));
		}
	}

	@Override
	public List<Task> getTasks() {
		List<smoslt.domain.Task> myTasks = new ArrayList<smoslt.domain.Task>();
		myTasks.addAll(tasks.values());
		return myTasks;
	}

	@Override
	public Set<smoslt.domain.Resource> getResources() {
		return this.resources;
	}

	// aTask.addAssignment(myassignment);
	// myassignment.getResource().addAssignment(myassignment);
	// List<Assignment> assigments = resource.getAssignments();
	// for (Assignment assignment : assigments) {
	// System.out.println("\t\t" + assignment.getDuration() + " "
	// + assignment.getUniqueId() + " "
	// + assignment.getUniqueIdString() + " "
	// + assignment.getTaskId() + " "
	// + assignment.getTask().getName());
	// System.out.println("\t\tRESOURCE: " +
	// assignment.getResourceName()
	// + " " + assignment.getResourceId() + " "
	// + assignment.getResourceAvailability() + " "
	// + assignment.getResource().getUniqueId());
	// }
	// }

	// resources = clone.getResourcePool().getResourceList();
	// for (Resource resource : resources) {
	// System.out.println("RESOURCE NAME: " + resource.getName()
	// + ", GROUP:" + resource.getGroup() + " "
	// + resource.getAssignments().size());

	// List<Assignment> assigments = resource.getAssignments();
	// for (Assignment assignment : assigments) {
	// System.out.println("\t\t" + assignment.getDuration() + " "
	// + assignment.getUniqueId() + " "
	// + assignment.getUniqueIdString() + " TASKID: "
	// + assignment.getTaskId() + " "
	// + assignment.getTask().getName());
	// }
	// for (Object task : clone.getTasks().toArray()) {
	// NormalTask aTask = (NormalTask) task;
	// System.out.println("TASK NAME: " + aTask.getName()
	// + ", TASKID:" + aTask.getId() + ", TASK:"
	// + aTask.getActualDuration() + ", TO: "
	// + aTask.getEffectiveWorkCalendar().getName());
	// System.out.println("TASK NOTES: " + aTask.getNotes());
	// // for(Resource resource: task.getDelegatedTo()){
	// // System.out.println("RESOURCE NAME: " + + ", GROUP:"
	// // +resource.getGroup());
	// // }
	// }

	// System.out.println(project.getTasks().get(0).getClass() +"  "+
	// project.getTasks().get(0).toString() + " " + resource.toString() +
	// " "+resource.getGroup());

}
