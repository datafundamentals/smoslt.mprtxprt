package smoslt.mprtxprt;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/** @author petecarapetyan */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring/mprtxprt-bundle-context.xml" }, inheritLocations = true)
public abstract class AbstractMprtxprtTest{
}