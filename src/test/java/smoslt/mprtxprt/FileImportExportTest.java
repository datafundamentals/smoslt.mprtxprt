package smoslt.mprtxprt;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import smoslt.domain.ProjectProfile;
import smoslt.domain.Schedule;
import smoslt.domain.ScheduleBuild;
import smoslt.domain.Task;

public class FileImportExportTest {

	@Test
	public void test() throws JsonParseException, JsonMappingException, IOException {
		/*
		 * Please note that the exported file DOES NOT include any resource
		 * assignments. This is by design. See note in
		 * ImportFromProjectLibre.java
		 */
		File file = new File("src/test/resources/fc601709.pod");
		File exportFile = new File("src/test/resources/exported.pod");
		ScheduleBuild scheduleBuild = new ImportFromProjectLibre(file);
		Schedule schedule = new Schedule(scheduleBuild);
		System.out.println("SCHEDULE HAS " + schedule.getAssignments().size()
				+ " ASSIGNMENTS");
		for (Task task : schedule.getTaskList()) {
			// System.out.println(task.getResourceSpecifications());
		}
		ObjectMapper objectMapper = new ObjectMapper();
		ProjectProfile projectProfile = objectMapper.readValue(new File("src/main/resources/META-INF/config/projectProfile_a.json"),
				ProjectProfile.class);
		schedule.setProjectProfile(projectProfile);
		ExportIntoProjectLibre exportIntoProjectLibre = new ExportIntoProjectLibre(
				schedule);
		exportIntoProjectLibre.go();
		exportIntoProjectLibre.export(exportFile);
		exportFile.delete();
	}

}
